
// MainDlg.h : 头文件
//

#pragma once


#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <iomanip>
using namespace std;

// CMainDlg 对话框
class CMainDlg : public CDialogEx
{
// 构造
public:
	CMainDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SCANFOLDERANDCLIPPING_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

private:
	void ScanFolder (CString dir_path, vector<CString>& vcPtsPath);
	bool SelectDecodeDir(CString& out_path);

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
