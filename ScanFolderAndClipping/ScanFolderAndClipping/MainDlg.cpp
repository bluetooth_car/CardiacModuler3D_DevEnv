
// MainDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ScanFolderAndClipping.h"
#include "MainDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainDlg 对话框

CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMainDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMainDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMainDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CMainDlg 消息处理程序

BOOL CMainDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMainDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMainDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMainDlg::OnBnClickedOk()
{
	CString dir_path;
	if (! SelectDecodeDir(dir_path)) {
		return;
	}

	BeginWaitCursor();
	vector<CString> vcPtsPath;
	ScanFolder(dir_path, vcPtsPath);
	EndWaitCursor();

	if (vcPtsPath.empty()) {
		MessageBox(_T("指定目录下未搜索到 jpg 文件！"));
	}

	CString info;
	info.Format(_T("共找到%d个文件\r\n请选择转储目录"), vcPtsPath.size());
	MessageBox(info);

	CString out_path;
	if (! SelectDecodeDir(out_path)) {
		return;
	}

	BeginWaitCursor();
	int i;
	for (i = 0; i < vcPtsPath.size(); i++) {

		CString out_filename;
		out_filename.Format(_T("%s\\%03d.bmp"), out_path, i+1);

		//CopyFile (vcPtsPath[i], out_filename, FALSE);

		theApp.ClipingImage(vcPtsPath[i], out_filename);
	}
	EndWaitCursor();
	//CDialogEx::OnOK();
}

void CMainDlg::ScanFolder (CString dir_path, vector<CString>& vcPtsPath)
{
	dir_path += _T("\\*");

	CFileFind finder;
	BOOL bWorking = finder.FindFile(dir_path);
	while (bWorking) {

		bWorking = finder.FindNextFile();
		CString sFileName = finder.GetFileName();

		if (finder.IsDirectory() &&
			"." != finder.GetFileName() &&
			".." != finder.GetFileName())//注意该句需要排除“.”“..”
        {
            //递归调用
			CString file_path = finder.GetFilePath();
            ScanFolder (file_path, vcPtsPath);
        }
        else
        {
            CString strFile = finder.GetFilePath();
            CString strFileName = finder.GetFileName();
			CString strFileExtension = strFileName.Right(3);
            if (_T(".") != strFileName && _T("..") != strFileName)
            {
                if (strFileExtension.CompareNoCase(_T("jpg")) == 0)
                {
					vcPtsPath.push_back(strFile);
                }
            }
        }
	}
}

bool CMainDlg::SelectDecodeDir(CString& out_path)
{
	TCHAR szPath[MAX_PATH];     //存放选择的目录路径   
	ZeroMemory(szPath, sizeof(szPath));

	BROWSEINFO bi;
	bi.hwndOwner = m_hWnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szPath;
	bi.lpszTitle = _T("请选择输入目录：");
	bi.ulFlags = 0;
	bi.lpfn = NULL;
	bi.lParam = 0;
	bi.iImage = 0;
	LPITEMIDLIST lp = SHBrowseForFolder(&bi);     

	if (lp && SHGetPathFromIDList(lp, szPath)) {
		//CString str;
		//str.Format(_T("选择的目录为 %s"), szPath);
		//MessageBox(str);
		out_path.Format(_T("%s"), szPath);
		return true;
	}
	else {
		if (lp) {
			CString str;
			str.Format(_T("无效的目录！"));
			MessageBox(str);
		}
		out_path = _T("");
		return false;
	}
}
