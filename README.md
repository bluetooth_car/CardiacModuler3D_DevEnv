# 3D心脏模型及开发环境

#### 介绍
制作3D心脏模型的开发工具系统，该工具链可以生成供ParaView使用的3D空间模型。

#### 使用说明
软件源码使用 Microsoft Visual Studio 2012 以上版本打开，由 VC++ 和 MFC 开发。

具体用法请参考《使用说明.pdf》文件中的详细说明。


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
