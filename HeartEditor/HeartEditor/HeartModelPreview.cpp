// HeartModelPreview.cpp : 实现文件
//

#include "stdafx.h"
#include "HeartEditor.h"
#include "HeartModelPreview.h"
#include "HeartEditorView.h"
#include "HeartEditorDoc.h"

#define USERMSG_DRAW_FINISH    (WM_USER + 1)

// CHeartModelPreview

IMPLEMENT_DYNCREATE(CHeartModelPreview, CFormView)

CHeartModelPreview::CHeartModelPreview()
	: CFormView(CHeartModelPreview::IDD)
{
	m_bDrawThreadRun = FALSE;
	m_hDrawThread = NULL;
}

CHeartModelPreview::~CHeartModelPreview()
{
}

void CHeartModelPreview::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CMB_CURRENT_ORGAN, m_cmbOrganType);
	DDX_Control(pDX, IDC_CHK_SHOW_ATRIUM, m_chkAtrium);
	DDX_Control(pDX, IDC_CHK_SHOW_CONDUCT, m_chkConduct);
	DDX_Control(pDX, IDC_CHK_SHOW_AUTORHY, m_chkAutorhy);
	DDX_Control(pDX, IDC_CHK_SHOW_INSULA, m_chkInsula);
	DDX_Control(pDX, IDC_CHK_SHOW_VESSEL, m_chkVessel);
	DDX_Control(pDX, IDC_CHK_SHOW_BLOOD, m_chkBlood);
	DDX_Control(pDX, IDC_CHK_SHOW_FAT, m_chkFat);
	DDX_Control(pDX, IDC_CHK_SHOW_SAC, m_chkSac);
	DDX_Control(pDX, IDC_CHK_SHOW_TRACHEA, m_chkTrachea);
	DDX_Control(pDX, IDC_CHK_SHOW_VENTRI, m_chkVentri);
	DDX_Control(pDX, IDC_STA_PREVIEW, m_staPreviewArea);
}

BEGIN_MESSAGE_MAP(CHeartModelPreview, CFormView)
	ON_CBN_SELCHANGE(IDC_CMB_CURRENT_ORGAN, &CHeartModelPreview::OnCbnSelchangeCmbCurrentOrgan)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_NORMAL, &CHeartModelPreview::OnBnClickedBtnViewpointNormal)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_BACK, &CHeartModelPreview::OnBnClickedBtnViewpointBack)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_LEFT_BACK, &CHeartModelPreview::OnBnClickedBtnViewpointLeftBack)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_LEFT, &CHeartModelPreview::OnBnClickedBtnViewpointLeft)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_LEFT_FRONT, &CHeartModelPreview::OnBnClickedBtnViewpointLeftFront)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_RIGHT_FRONT, &CHeartModelPreview::OnBnClickedBtnViewpointRightFront)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_RIGHT, &CHeartModelPreview::OnBnClickedBtnViewpointRight)
	ON_BN_CLICKED(IDC_BTN_VIEWPOINT_RIGHT_BACK, &CHeartModelPreview::OnBnClickedBtnViewpointRightBack)
	ON_BN_CLICKED(IDC_BTN_STOP_DRAW, &CHeartModelPreview::OnBnClickedBtnStopDraw)
	ON_MESSAGE(USERMSG_DRAW_FINISH, &CHeartModelPreview::OnDrawFinish)
END_MESSAGE_MAP()


// CHeartModelPreview 诊断

#ifdef _DEBUG
void CHeartModelPreview::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CHeartModelPreview::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CHeartModelPreview 消息处理程序


void CHeartModelPreview::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_cmbOrganType.InitBox();
	m_chkAtrium.SetCheck(1);

	theApp.m_pHeartModelPreview = this;
}


void CHeartModelPreview::OnCbnSelchangeCmbCurrentOrgan()
{
	int index = m_cmbOrganType.GetCurSel();

	if (theApp.m_pHeartEditorView) {

		char organ = ORGAN_ZERO + 1 + index;
		theApp.m_pHeartEditorView->SetSelectedOrgan(organ);

		// 给相应的组织类型打上勾
		switch (organ) {
		case ORGAN_ATRIUM:	m_chkAtrium.SetCheck(1);	break;
		case ORGAN_CONDUCT:	m_chkConduct.SetCheck(1);	break;
		case ORGAN_AUTORHY:	m_chkAutorhy.SetCheck(1);	break;
		case ORGAN_INSULA:	m_chkInsula.SetCheck(1);	break;
		case ORGAN_VESSEL:	m_chkVessel.SetCheck(1);	break;
		case ORGAN_BLOOD:	m_chkBlood.SetCheck(1);		break;
		case ORGAN_FAT:		m_chkFat.SetCheck(1);		break;
		case ORGAN_SAC:		m_chkSac.SetCheck(1);		break;
		case ORGAN_TRACHEA:	m_chkTrachea.SetCheck(1);	break;
		case ORGAN_VENTRI:	m_chkVentri.SetCheck(1);	break;
		}
	}
}

void CHeartModelPreview::UpdateOrganCheck(BOOL bChkAtrium,
		BOOL bChkConduct,
		BOOL bChkAutorhy,
		BOOL bChkInsula,
		BOOL bChkVessel,
		BOOL bChkBlood,
		BOOL bChkFat,
		BOOL bChkSac,
		BOOL bChkTrachea,
		BOOL bChkVentri)
{
	m_chkAtrium.SetCheck(bChkAtrium);
	m_chkConduct.SetCheck(bChkConduct);
	m_chkAutorhy.SetCheck(bChkAutorhy);
	m_chkInsula.SetCheck(bChkInsula);
	m_chkVessel.SetCheck(bChkVessel);
	m_chkBlood.SetCheck(bChkBlood);
	m_chkFat.SetCheck(bChkFat);
	m_chkSac.SetCheck(bChkSac);
	m_chkTrachea.SetCheck(bChkTrachea);
	m_chkVentri.SetCheck(bChkVentri);

	m_bChkAtrium  = bChkAtrium;
	m_bChkConduct = bChkConduct;
	m_bChkAutorhy = bChkAutorhy;
	m_bChkInsula  = bChkInsula;
	m_bChkVessel  = bChkVessel;
	m_bChkBlood   = bChkBlood;
	m_bChkFat     = bChkFat;
	m_bChkSac     = bChkSac;
	m_bChkTrachea = bChkTrachea;
	m_bChkVentri  = bChkVentri;
}


const int tile_w = 2;
const int tile_h = 1;
const int tile_d = 1;
const int half_w = tile_w / 2;
const int half_h = (tile_h + tile_d) / 2;
void CHeartModelPreview::DrawOragn(CDC* pdc, char organ,
								   int offset_x, int start_y,
								   int x, int y, BOOL half_grid)
{
	if (ORGAN_ZERO == organ) return;
	else if (ORGAN_ATRIUM  == organ && ! m_bChkAtrium) return;
	else if (ORGAN_CONDUCT == organ && ! m_bChkConduct) return;
	else if (ORGAN_AUTORHY == organ && ! m_bChkAutorhy) return;
	else if (ORGAN_INSULA  == organ && ! m_bChkInsula) return;
	else if (ORGAN_VESSEL  == organ && ! m_bChkVessel) return;
	else if (ORGAN_BLOOD   == organ && ! m_bChkBlood) return;
	else if (ORGAN_FAT     == organ && ! m_bChkFat) return;
	else if (ORGAN_SAC     == organ && ! m_bChkSac) return;
	else if (ORGAN_TRACHEA == organ && ! m_bChkTrachea) return;
	else if (ORGAN_VENTRI  == organ && ! m_bChkVentri) return;

	CBrush& face = GetOrganBrush(organ);
	CBrush& side = GetSideBrush(organ);

	//  --------
	// |        |
	// |  face  |
	// |        |
	//  --------
	// |  side  |
	//  --------
	int tile_x = tile_w * x + offset_x + (half_grid ? half_w : 0);
	int tile_y = tile_h * y + start_y + (half_grid ? half_h : 0);

	CRect rc_face(tile_x,
		tile_y, 
		tile_x + tile_w,
		tile_y + tile_h);
	CRect rc_side(tile_x,
		tile_y + tile_d,
		tile_x + tile_w,
		tile_y + tile_h + tile_d);

	pdc->FillRect(&rc_side, &side);
	pdc->FillRect(&rc_face, &face);
}

DWORD WINAPI DrawThread(LPVOID arg)
{
	CHeartModelPreview* pView = (CHeartModelPreview *) arg;
	pView->BeginWaitCursor();

	CDC* pdc = pView->GetDC();
	CRect rc;
	CBrush brBlack(RGB(0,0,0));
	pView->m_staPreviewArea.GetWindowRect(&rc);
	pView->ScreenToClient(&rc);
	pdc->FillRect(&rc, &brBlack);

	const int offset_x = rc.left;
	// 模型左下角的位置，因为模型是一层一层从下往上绘制的。
	int start_y = rc.bottom - MODEL_H * tile_h - 1;

	// 绘制心脏模型(从底部往上画，一直画到当前操作的层为止)
	if (theApp.m_pHeartEditorView) {
		CHeartEditorDoc* pDoc = theApp.m_pHeartEditorView->GetDocument();
		int curIndex = max (0, pDoc->GetCurImageIndex());

		int x, y, z;
		for (z = MODEL_D - 1; z >= curIndex; z--) { 

			if (! pView->m_bDrawThreadRun) {
				break; // 退出线程
			}

			switch (pView->m_enDrawMode)
			{
			case VP_NORMAL:
				for (x = 0; x < MODEL_W; x++) {
					for (y = 0; y < MODEL_H; y++) {

						char organ = pDoc->GetOrgan(x, y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, x, y);
					}
				}
				break;

			case VP_BACK:
				for (x = 0; x < MODEL_W; x++) {
					for (y = 0; y < MODEL_H; y++) {

						char organ = pDoc->GetOrgan(MODEL_W - x -1, MODEL_W - y -1, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, x, y);
					}
				}
				break;

			case VP_LEFT:
				for (x = 0; x < MODEL_H; x++) {
					for (y = 0; y < MODEL_W; y++) {

						char organ = pDoc->GetOrgan(MODEL_W - y -1, x, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, x, y);
					}
				}
				break;

			case VP_RIGHT:
				for (x = 0; x < MODEL_H; x++) {
					for (y = 0; y < MODEL_W; y++) {

						char organ = pDoc->GetOrgan(y, MODEL_H - x -1, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, x, y);
					}
				}
				break;

			case VP_LEFT_FRONT:
				for (y = 0; y < MODEL_W; y++) {
					for (x = 0; x <= min (MODEL_H - 1, y); x++) {
						int data_x = MODEL_W - 1 - y + x;
						int data_y = x;
						int show_y = y / 2;
						int show_x = x + MODEL_H / 2 - show_y;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, (y+1) % 2);
					}
				}
				for (y = 1; y < MODEL_H - 1; y++) {
					for (x = 0; x <= min (MODEL_W - 1, MODEL_H - 1 - y); x++) {
						int data_x = x;
						int data_y = y + x;
						int show_y = (MODEL_H + y - 1) / 2;
						int show_x = x + y / 2 + 1;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, y % 2);
					}
				}
				break;

			case VP_RIGHT_FRONT:
				for (y = 0; y < MODEL_H; y++) {
					for (x = 0; x <= min (MODEL_W - 1, y); x++) {
						int data_x = x;
						int data_y = y - x;
						int show_y = y / 2;
						int show_x = x + MODEL_W / 2 - show_y;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, (y+1) % 2);
					}
				}
				for (y = 1; y < MODEL_W - 1; y++) {
					for (x = 0; x <= min (MODEL_H - 1, MODEL_W - 1 - y); x++) {
						int data_x = y + x;
						int data_y = MODEL_H - 1 - x;
						int show_y = (MODEL_H + y - 1) / 2;
						int show_x = x + y / 2 + 1;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, y % 2);
					}
				}
				break;

				case VP_LEFT_BACK:
				for (y = 0; y < MODEL_H; y++) {
					for (x = 0; x <= min (MODEL_W - 1, y); x++) {
						int data_x = MODEL_W - 1 - x;
						int data_y = MODEL_H - 1 - y + x;
						int show_y = y / 2;
						int show_x = x + MODEL_H / 2 - show_y;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, (y+1) % 2);
					}
				}
				for (y = 1; y < MODEL_W - 1; y++) {
					for (x = 0; x <= min (MODEL_H - 1, MODEL_W - 1 - y); x++) {
						int data_x = MODEL_W - 1 - y - x;
						int data_y = x;
						int show_y = (MODEL_H + y - 1) / 2;
						int show_x = x + y / 2 + 1;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, y % 2);
					}
				}
				break;

				case VP_RIGHT_BACK:
				for (y = 0; y < MODEL_W; y++) {
					for (x = 0; x <= min (MODEL_H - 1, y); x++) {
						int data_x = y - x;
						int data_y = MODEL_H - 1 - x;
						int show_y = y / 2;
						int show_x = x + MODEL_H / 2 - show_y;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, (y+1) % 2);
					}
				}
				for (y = 1; y < MODEL_H - 1; y++) {
					for (x = 0; x <= min (MODEL_W - 1, MODEL_H - 1 - y); x++) {
						int data_x = MODEL_W - 1 - x;
						int data_y = MODEL_H - 1 - y - x;
						int show_y = (MODEL_H + y - 1) / 2;
						int show_x = x + y / 2 + 1;
						char organ = pDoc->GetOrgan(data_x, data_y, z);
						pView->DrawOragn(pdc, organ, offset_x, start_y, show_x, show_y, y % 2);
					}
				}
				break;
			}

			start_y -= tile_d;
			
		}
	}

	pView->ReleaseDC(pdc);
	pView->EndWaitCursor();
	pView->PostMessage(USERMSG_DRAW_FINISH);
	return 0;
}

void CHeartModelPreview::Renew(EnViewpointMode mode)
{
	// 首先停止现有的绘图
	if (m_bDrawThreadRun) {
		// 停止绘图线程
		StopDrawThread();
	}
	else {

		m_bChkAtrium  = m_chkAtrium.GetCheck();
		m_bChkConduct = m_chkConduct.GetCheck();
		m_bChkAutorhy = m_chkAutorhy.GetCheck();
		m_bChkInsula  = m_chkInsula.GetCheck();
		m_bChkVessel  = m_chkVessel.GetCheck();
		m_bChkBlood   = m_chkBlood.GetCheck();
		m_bChkFat     = m_chkFat.GetCheck();
		m_bChkSac     = m_chkSac.GetCheck();
		m_bChkTrachea = m_chkTrachea.GetCheck();
		m_bChkVentri  = m_chkVentri.GetCheck();

		m_chkAtrium.EnableWindow(FALSE);
		m_chkConduct.EnableWindow(FALSE);
		m_chkAutorhy.EnableWindow(FALSE);
		m_chkInsula.EnableWindow(FALSE);
		m_chkVessel.EnableWindow(FALSE);
		m_chkBlood.EnableWindow(FALSE);
		m_chkFat.EnableWindow(FALSE);
		m_chkSac.EnableWindow(FALSE);
		m_chkTrachea.EnableWindow(FALSE);
		m_chkVentri.EnableWindow(FALSE);

		// 启动绘图线程
		m_enDrawMode = mode;
		m_bDrawThreadRun = TRUE;
		m_hDrawThread = CreateThread(NULL,0,DrawThread,this,0,NULL);
	}
}

void CHeartModelPreview::StopDrawThread(void)
{
	if (m_bDrawThreadRun) {
		// 停止绘图线程
		m_bDrawThreadRun = FALSE;
		WaitForSingleObject(m_hDrawThread, INFINITE);
		CloseHandle(m_hDrawThread);
		m_hDrawThread = NULL;

		m_chkAtrium.EnableWindow(TRUE);
		m_chkConduct.EnableWindow(TRUE);
		m_chkAutorhy.EnableWindow(TRUE);
		m_chkInsula.EnableWindow(TRUE);
		m_chkVessel.EnableWindow(TRUE);
		m_chkBlood.EnableWindow(TRUE);
		m_chkFat.EnableWindow(TRUE);
		m_chkSac.EnableWindow(TRUE);
		m_chkTrachea.EnableWindow(TRUE);
		m_chkVentri.EnableWindow(TRUE);
	}
}

void CHeartModelPreview::OnBnClickedBtnViewpointNormal()
{
	Renew();
}

void CHeartModelPreview::OnBnClickedBtnViewpointBack()
{
	Renew(VP_BACK);
}

void CHeartModelPreview::OnBnClickedBtnViewpointLeftBack()
{
	Renew(VP_LEFT_BACK);
}

void CHeartModelPreview::OnBnClickedBtnViewpointLeft()
{
	Renew(VP_LEFT);
}

void CHeartModelPreview::OnBnClickedBtnViewpointLeftFront()
{
	Renew(VP_LEFT_FRONT);
}

void CHeartModelPreview::OnBnClickedBtnViewpointRightFront()
{
	Renew(VP_RIGHT_FRONT);
}

void CHeartModelPreview::OnBnClickedBtnViewpointRight()
{
	Renew(VP_RIGHT);
}

void CHeartModelPreview::OnBnClickedBtnViewpointRightBack()
{
	Renew(VP_RIGHT_BACK);
}

void CHeartModelPreview::OnBnClickedBtnStopDraw()
{
	StopDrawThread();
}

afx_msg LRESULT CHeartModelPreview::OnDrawFinish(WPARAM wParam, LPARAM lParam)
{
	m_bDrawThreadRun = FALSE;
	CloseHandle(m_hDrawThread);
	m_hDrawThread = NULL;

	m_chkAtrium.EnableWindow(TRUE);
	m_chkConduct.EnableWindow(TRUE);
	m_chkAutorhy.EnableWindow(TRUE);
	m_chkInsula.EnableWindow(TRUE);
	m_chkVessel.EnableWindow(TRUE);
	m_chkBlood.EnableWindow(TRUE);
	m_chkFat.EnableWindow(TRUE);
	m_chkSac.EnableWindow(TRUE);
	m_chkTrachea.EnableWindow(TRUE);
	m_chkVentri.EnableWindow(TRUE);

	return 0;
}
