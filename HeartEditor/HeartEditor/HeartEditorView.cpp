
// HeartEditorView.cpp : CHeartEditorView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "HeartEditor.h"
#endif

#include "HeartEditorDoc.h"
#include "HeartEditorView.h"

#include <vector>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHeartEditorView
void CHeartEditorView::PrepareSliceDC(CHeartEditorDoc* pDoc)
{
	m_dcSlice.BitBlt(0, 0, BKGND_W, BKGND_H, &m_dcBkgnd, 0, 0, SRCCOPY);
	m_dcSlice.TextOut(m_ptTopLeftTile.x + 10,
					  m_ptTopLeftTile.y + 10,
					  m_sCurImageName);

	int x, y;
	for (x = 0; x < MODEL_W; x++) {
		for (y = 0; y < MODEL_H; y++) {

			char organ = pDoc->GetOrgan(x, y);
			if (ORGAN_ZERO == organ)
				continue;

			CBrush& br = GetOrganBrush(organ);

			int tile_x = ORGAN_TILE_PIXEL_W * x;
			int tile_y = ORGAN_TILE_PIXEL_H * y;
			CRect rc(tile_x, tile_y, 
				tile_x + ORGAN_TILE_PIXEL_W,
				tile_y + ORGAN_TILE_PIXEL_H);

			m_dcSlice.FillRect(&rc, &br);
		}
	}
}


IMPLEMENT_DYNCREATE(CHeartEditorView, CScrollView)

BEGIN_MESSAGE_MAP(CHeartEditorView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SETCURSOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CHeartEditorView 构造/析构

CHeartEditorView::CHeartEditorView()
{
	theApp.m_pHeartEditorView = this;

	run_once = TRUE;//保证OnDraw函数中的初始化段落只执行一次

	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;

	m_ptTopLeftTile = CPoint(0, 0);
}

CHeartEditorView::~CHeartEditorView()
{
}

BOOL CHeartEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CScrollView::PreCreateWindow(cs);
}

void CHeartEditorView::SetCurImage(int nImgIndex)
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CString sImgFilename = theApp.GetImagePathByIndex(nImgIndex);
	if (LoadBkgndImage(sImgFilename)) {

		m_sCurImageName = GetFileName(sImgFilename);
		pDoc->SetCurImage(nImgIndex);

		Invalidate();
	}
	else {
		CString errInfo;
		errInfo.Format(_T("文件\n%s\n打开失败！"), sImgFilename);
		MessageBox(errInfo);
	}
}

void CHeartEditorView::SetSelectedOrgan(char organ)
{
	if (organ > 0 && organ < ORGAN_MAX)
	{
		CHeartEditorDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		if (!pDoc)
			return;

		pDoc->SetSelectedOrgan(organ);
	}
}

BOOL CHeartEditorView::LoadBkgndImage(CString sImgFilename)
{
	if (run_once) //此时CDC还没有初始化，不能实现此功能
		return FALSE;

	HBITMAP hbmp = (HBITMAP)LoadImage(NULL,sImgFilename,
		IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//地图单元格位图

	if (NULL == hbmp)//打开图片出错
		return FALSE;

	HGDIOBJ hOld = m_dcTemp.SelectObject(hbmp);
	m_dcBkgnd.StretchBlt(0, 0, BKGND_W, BKGND_H,
		&m_dcTemp, 0, 0, MODEL_W, MODEL_H, SRCCOPY);
	m_dcTemp.SelectObject(hOld);

	return TRUE;
}

// CHeartEditorView 绘制

void CHeartEditorView::OnDraw(CDC* pDC)
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	if (run_once) {//程序第一次运行所需的操作

		CBitmap bmp;
		bmp.CreateCompatibleBitmap(pDC, BKGND_W, BKGND_H);
		m_dcBkgnd.CreateCompatibleDC(pDC);
		m_dcBkgnd.SelectObject(bmp);

		CBitmap bmp2;
		bmp2.CreateCompatibleBitmap(pDC, BKGND_W, BKGND_H);
		m_dcSlice.CreateCompatibleDC(pDC);
		m_dcSlice.SelectObject(bmp2);

		m_dcTemp.CreateCompatibleDC(pDC);

		run_once = FALSE;
	}

	PrepareSliceDC(pDoc);
	pDC->BitBlt(0, 0, BKGND_W, BKGND_H, &m_dcSlice, 0, 0, SRCCOPY);
}

void CHeartEditorView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	m_ptTopLeftTile = CPoint(0, 0);

	CSize sizeTotal;

	// 计算此视图的合计大小
	sizeTotal.cx = BKGND_W;
	sizeTotal.cy = BKGND_H;
	SetScrollSizes(MM_TEXT, sizeTotal);
}


// CHeartEditorView 诊断

#ifdef _DEBUG
void CHeartEditorView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CHeartEditorView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CHeartEditorDoc* CHeartEditorView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHeartEditorDoc)));
	return (CHeartEditorDoc*)m_pDocument;
}
#endif //_DEBUG

void CHeartEditorView::SetOneTile(int x, int y, char organ)
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	int X = x / ORGAN_TILE_PIXEL_W;
	int Y = y / ORGAN_TILE_PIXEL_H;

	if ((X > MODEL_W-1) || (X < 0) || (Y > MODEL_H-1) || (Y < 0)) {
		return;
	}

	if (pDoc->GetOrgan (X, Y) != organ)
	{
		pDoc->SetOperateFlag(); // 给undo发出指示，表示这是一次操作
		pDoc->SetOrgan (X, Y, organ);

		Invalidate();
	}
}

char CHeartEditorView::GetOrgan(CPoint pt)
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return ORGAN_MAX;

	int X = pt.x / ORGAN_TILE_PIXEL_W;
	int Y = pt.y / ORGAN_TILE_PIXEL_H;

	if ((X > MODEL_W-1) || (X < 0) || (Y > MODEL_H-1) || (Y < 0)) {
		return ORGAN_MAX; // 表示出错，这个值是不应该出现的
	}

	return pDoc->GetOrgan(X, Y);
}

// CHeartEditorView 消息处理程序
void CHeartEditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	if (ORGAN_ZERO == pDoc->GetSelectedOrgan()) {
		MessageBox (_T("请首先在右侧窗口选择当前的组织类型"));
		return;
	}

	//根据滚动条位置修正鼠标落点
	point.Offset (m_ptTopLeftTile);

	if (pDoc->m_bFillMode) { // 填充连续区域

		char newOrgan = pDoc->GetSelectedOrgan();
		char oldOrgan = GetOrgan(point);
		if (newOrgan != oldOrgan)
			FullFill (point, newOrgan, oldOrgan);
		return;
	}

	// 标记鼠标左键已经落下
	m_bLButtonDown = TRUE;
	m_bRButtonDown = FALSE;

	// 将鼠标命中的单元设置为当前选中的格子
	SetOneTile (point.x, point.y, pDoc->GetSelectedOrgan());

	//CView::OnLButtonDown(nFlags, point);
}

void CHeartEditorView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	//右键点击清除格子
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	//根据滚动条位置修正鼠标落点
	point.Offset (m_ptTopLeftTile);

	if (pDoc->m_bFillMode) { // 清除连续区域

		char newOrgan = ORGAN_ZERO;
		char oldOrgan = GetOrgan(point);
		if (newOrgan != oldOrgan)
			FullFill (point, newOrgan, oldOrgan);
		return;
	}

	// 标记鼠标右键已经落下
	m_bLButtonDown = FALSE;
	m_bRButtonDown = TRUE;

	// 清掉鼠标命中的单元
	SetOneTile (point.x, point.y, ORGAN_ZERO);

	//CView::OnRButtonDown(nFlags, point);
}

void CHeartEditorView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;
	
	//CView::OnLButtonUp(nFlags, point);
}

void CHeartEditorView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;
	
	//CView::OnRButtonUp(nFlags, point);
}

void CHeartEditorView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	if (pDoc->m_bFillMode) {
		SetCursor(pDoc->m_curFullFill);
		return;
	}

	//根据滚动条位置修正鼠标落点
	point.Offset (m_ptTopLeftTile);

	// 鼠标左键按住，拖动时为放置操作
	if (m_bLButtonDown)
	{
		SetOneTile (point.x, point.y, pDoc->GetSelectedOrgan());
	}
	// 鼠标右键按住，拖动时为清除操作
	else if (m_bRButtonDown)
	{
		SetOneTile (point.x, point.y, ORGAN_ZERO);
	}
	
	//CView::OnMouseMove(nFlags, point);
}

BOOL CHeartEditorView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;

	if(zDelta < 0) {
		OnVScroll(SB_PAGEDOWN, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
	}
	else if (zDelta > 0) {
		OnVScroll(SB_PAGEUP, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
	}

	m_ptTopLeftTile = CPoint(GetScrollPos(SB_HORZ),
							 GetScrollPos(SB_VERT));
	Invalidate(0);
	return TRUE;

	//return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

void CHeartEditorView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	m_ptTopLeftTile = CPoint(GetScrollPos(SB_HORZ),
							 GetScrollPos(SB_VERT));

	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CHeartEditorView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	m_ptTopLeftTile = CPoint(GetScrollPos(SB_HORZ),
							 GetScrollPos(SB_VERT));

	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
}

BOOL CHeartEditorView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;//避免背景闪烁
	//return CScrollView::OnEraseBkgnd(pDC);
}

BOOL CHeartEditorView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return TRUE;

	if (pDoc->m_bFillMode) {
		return TRUE; //表示已经设置过光标了
	}
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void FloodFill(CHeartEditorDoc* pDoc, int X, int Y, char newOrgan, char oldOrgan);
void CHeartEditorView::FullFill(CPoint point, char newOrgan, char oldOrgan)
{
	int X = point.x / ORGAN_TILE_PIXEL_W;
	int Y = point.y / ORGAN_TILE_PIXEL_H;

	if ((X > MODEL_W-1) || (X < 0) || (Y > MODEL_H-1) || (Y < 0)) {
		return;
	}

	CHeartEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// 给undo发出指示，表示开始一次操作
	pDoc->SetOperateFlag();

	BeginWaitCursor();
	FloodFill (pDoc, X, Y, newOrgan, oldOrgan);
	Invalidate();
	EndWaitCursor();
}

// 递归版本的泛洪填充函数（算法正确，但是会造成栈溢出）
//void FloodFill(CHeartEditorDoc* pDoc, int X, int Y, char newOrgan, char oldOrgan)
//{
//	if (X >= 0 && X < MODEL_W &&
//		Y >= 0 && Y < MODEL_H &&
//		pDoc->GetOrgan (X, Y) == oldOrgan &&
//		pDoc->GetOrgan (X, Y) != newOrgan)
//	{
//		pDoc->SetOrgan (X, Y, newOrgan);
//
//		FloodFill (pDoc, X + 1, Y    , newOrgan, oldOrgan);
//		FloodFill (pDoc, X - 1, Y    , newOrgan, oldOrgan);
//		FloodFill (pDoc, X    , Y + 1, newOrgan, oldOrgan);
//		FloodFill (pDoc, X    , Y - 1, newOrgan, oldOrgan);
//	}
//}

// 循环版本的泛洪填充函数
void FloodFill(CHeartEditorDoc* pDoc, int X, int Y, char newOrgan, char oldOrgan)
{
	vector <CPoint> vc1;
	vector <CPoint> vc2;
	vector <CPoint>::iterator it;

	CPoint pt(X, Y);
	vc1.push_back(pt);

	while (1) {

		// 判断是否可填充，如果满足填充条件，则填充
		// 然后将已填充点周围的四个点放入容器（四方向泛洪）
		for (it = vc1.begin(); it != vc1.end(); it++) {

			CPoint& pt = *(it._Ptr);
			if (pt.x >= 0 && pt.x < MODEL_W &&
				pt.y >= 0 && pt.y < MODEL_H &&
				pDoc->GetOrgan (pt.x, pt.y) == oldOrgan &&
				pDoc->GetOrgan (pt.x, pt.y) != newOrgan)
			{
				pDoc->SetOrgan (pt.x, pt.y, newOrgan);

				vc2.push_back (CPoint(pt.x + 1, pt.y    ));
				vc2.push_back (CPoint(pt.x - 1, pt.y    ));
				vc2.push_back (CPoint(pt.x    , pt.y + 1));
				vc2.push_back (CPoint(pt.x    , pt.y - 1));
			}
		}

		// 检查容器vc2的长度，如果为0，则表示泛洪结束
		if (vc2.empty()) break;

		// vc1中的点用完可以删掉了
		vc1.clear();

		// 交换容器vc1和vc2
		vc1.swap(vc2);
	}
}

