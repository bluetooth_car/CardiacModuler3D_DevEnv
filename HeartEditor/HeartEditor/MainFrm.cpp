
// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "HeartEditor.h"

#include "MainFrm.h"
#include "HeartImageView.h"
#include "HeartEditorView.h"
#include "HeartModelPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO: 在此添加成员初始化代码
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}

	// TODO: 如果不需要可停靠工具栏，则删除这三行
	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(&m_wndToolBar);

	::SetProp(this->m_hWnd, ::AfxGetAppName(), (HANDLE)1);
	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	//将窗口分为一行三列
	DWORD dwStyle = WS_CHILD | WS_VISIBLE;
	if (!m_wndSplitter.CreateStatic(this, 1, 3, dwStyle)) {
		return FALSE;
	}

	//指定每个窗口的位置及初始大小CHeartLayerList//CHeartEditorView//CHeartModelPreview
	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CHeartImageView), CSize(HEART_LAYTER_VIEW_W, HEART_LAYTER_VIEW_H), pContext) ||
		!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CHeartEditorView), CSize(LAYER_EDIT_VIEW_W, LAYER_EDIT_VIEW_H), pContext) ||
		!m_wndSplitter.CreateView(0, 2, RUNTIME_CLASS(CHeartModelPreview), CSize(HEART_MODEL_VIEW_W, HEART_MODEL_VIEW_H), pContext))
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}
	return TRUE;

}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		 | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 消息处理程序

