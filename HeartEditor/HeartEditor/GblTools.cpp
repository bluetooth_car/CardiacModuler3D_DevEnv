// GblTools.cpp
#include "stdafx.h"


// 获得组织的颜色
COLORREF GetOrganColor (char organ)
{
	switch (organ)
	{
	case ORGAN_ZERO:	return CR_ZERO;
	case ORGAN_ATRIUM:	return CR_ATRIUM;
	case ORGAN_CONDUCT:	return CR_CONDUCT;
	case ORGAN_AUTORHY:	return CR_AUTORHY;
	case ORGAN_INSULA:	return CR_INSULA;
	case ORGAN_VESSEL:	return CR_VESSEL;
	case ORGAN_BLOOD:	return CR_BLOOD;
	case ORGAN_FAT:		return CR_FAT;
	case ORGAN_SAC:		return CR_SAC;
	case ORGAN_TRACHEA: return CR_TRACHEA;
	case ORGAN_VENTRI:  return CR_VENTRI;
	default:	return CR_ZERO;
	};
}

// 获得组织侧面的颜色
COLORREF GetSideColor (char organ)
{
	switch (organ)
	{
	case ORGAN_ZERO:	return SIDE_CR_ZERO;
	case ORGAN_ATRIUM:	return SIDE_CR_ATRIUM;
	case ORGAN_CONDUCT:	return SIDE_CR_CONDUCT;
	case ORGAN_AUTORHY:	return SIDE_CR_AUTORHY;
	case ORGAN_INSULA:	return SIDE_CR_INSULA;
	case ORGAN_VESSEL:	return SIDE_CR_VESSEL;
	case ORGAN_BLOOD:	return SIDE_CR_BLOOD;
	case ORGAN_FAT:		return SIDE_CR_FAT;
	case ORGAN_SAC:		return SIDE_CR_SAC;
	case ORGAN_TRACHEA: return SIDE_CR_TRACHEA;
	case ORGAN_VENTRI:  return SIDE_CR_VENTRI;
	default:	return SIDE_CR_ZERO;
	};
}

CBrush brZero(CR_ZERO);

CBrush brOrganAtrium(CR_ATRIUM);
CBrush brOragnConduct(CR_CONDUCT);
CBrush brOrganAutorhy(CR_AUTORHY);
CBrush brOrganInsula(CR_INSULA);
CBrush brOrganVessel(CR_VESSEL);
CBrush brOrganBlood(CR_BLOOD);
CBrush brOrganFat(CR_FAT);
CBrush brOrganSac(CR_SAC);
CBrush brOrganTrachea(CR_TRACHEA);
CBrush brOrganVentri(CR_VENTRI);

CBrush brSideAtrium(SIDE_CR_ATRIUM);
CBrush brSideConduct(SIDE_CR_CONDUCT);
CBrush brSideAutorhy(SIDE_CR_AUTORHY);
CBrush brSideInsula(SIDE_CR_INSULA);
CBrush brSideVessel(SIDE_CR_VESSEL);
CBrush brSideBlood(SIDE_CR_BLOOD);
CBrush brSideFat(SIDE_CR_FAT);
CBrush brSideSac(SIDE_CR_SAC);
CBrush brSideTrachea(SIDE_CR_TRACHEA);
CBrush brSideVentri(SIDE_CR_VENTRI);

CBrush& GetOrganBrush (char organ)
{
	switch (organ)
	{
	case ORGAN_ZERO:	return brZero;
	case ORGAN_ATRIUM:	return brOrganAtrium;
	case ORGAN_CONDUCT:	return brOragnConduct;
	case ORGAN_AUTORHY:	return brOrganAutorhy;
	case ORGAN_INSULA:	return brOrganInsula;
	case ORGAN_VESSEL:	return brOrganVessel;
	case ORGAN_BLOOD:	return brOrganBlood;
	case ORGAN_FAT:		return brOrganFat;
	case ORGAN_SAC:		return brOrganSac;
	case ORGAN_TRACHEA: return brOrganTrachea;
	case ORGAN_VENTRI:  return brOrganVentri;
	default:	return brZero;
	};
}

CBrush& GetSideBrush (char organ)
{
	switch (organ)
	{
	case ORGAN_ZERO:	return brZero;
	case ORGAN_ATRIUM:	return brSideAtrium;
	case ORGAN_CONDUCT:	return brSideConduct;
	case ORGAN_AUTORHY:	return brSideAutorhy;
	case ORGAN_INSULA:	return brSideInsula;
	case ORGAN_VESSEL:	return brSideVessel;
	case ORGAN_BLOOD:	return brSideBlood;
	case ORGAN_FAT:		return brSideFat;
	case ORGAN_SAC:		return brSideSac;
	case ORGAN_TRACHEA: return brSideTrachea;
	case ORGAN_VENTRI:  return brSideVentri;
	default:	return brZero;
	};
}