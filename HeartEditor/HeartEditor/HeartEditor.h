
// HeartEditor.h : HeartEditor 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// CMainApp:
// 有关此类的实现，请参阅 HeartEditor.cpp
//
class CHeartImageView;
class CHeartEditorView;
class CHeartModelPreview;
class CMainApp : public CWinApp
{
public:
	CMainApp();

public:
	CHeartImageView*     m_pHeartImageView;
	CHeartEditorView*    m_pHeartEditorView;
	CHeartModelPreview*  m_pHeartModelPreview;

	CString GetImagePathByIndex(int index);
	void WasteMessage(void);


// 重写
public:
	virtual BOOL InitInstance();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMainApp theApp;
