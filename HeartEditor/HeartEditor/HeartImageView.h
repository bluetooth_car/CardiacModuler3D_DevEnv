#pragma once


// CHeartImageView 视图
class CHeartImageView : public CListView
{
	DECLARE_DYNCREATE(CHeartImageView)

private:
	CImageList m_lstHeartImage;


protected:
	CHeartImageView();           // 动态创建所使用的受保护的构造函数
	virtual ~CHeartImageView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
};


