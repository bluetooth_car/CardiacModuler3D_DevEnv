
// HeartEditorDoc.cpp : CHeartEditorDoc 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "HeartEditor.h"
#endif

#include "HeartEditorDoc.h"
#include "HeartModelPreview.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CHeartEditorDoc

IMPLEMENT_DYNCREATE(CHeartEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CHeartEditorDoc, CDocument)
	ON_COMMAND(ID_EXPORT_PARAVIEW, &CHeartEditorDoc::OnExportParaview)
	ON_COMMAND(ID_EDIT_COPY, &CHeartEditorDoc::OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, &CHeartEditorDoc::OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &CHeartEditorDoc::OnUpdateEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &CHeartEditorDoc::OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_UNDO, &CHeartEditorDoc::OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FILL, &CHeartEditorDoc::OnUpdateEditFill)
	ON_COMMAND(ID_EDIT_FILL, &CHeartEditorDoc::OnEditFill)
END_MESSAGE_MAP()


// CHeartEditorDoc 构造/析构

CHeartEditorDoc::CHeartEditorDoc()
{
	m_bFillMode = FALSE;

	m_nCurImageIndex = -1;
	m_nCopyFromPage  = -1;
	m_chSelectedOrgan = ORGAN_ATRIUM;

	m_curArrow = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	m_curFullFill = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_FULLFILL));

	ResetBkup();
}

CHeartEditorDoc::~CHeartEditorDoc()
{
}

//用于清空历史数据，并使m_can_undo指示失效。
void CHeartEditorDoc::ResetBkup()
{
	//复位Undo队列指针
	m_bkup_counter = 0;
	m_bkup_top = 0;
	m_can_undo = FALSE;
}

//向地图矩阵写入一个数据
void CHeartEditorDoc::SaveToBkup(char value, WORD x, WORD y, WORD z, char flag)
{
	m_bkup_counter = ((m_bkup_counter == UNDO_CAPACITY) ?
					  m_bkup_counter : (++m_bkup_counter));//counter最大值等于CAPACITY
	m_bkup_top = ((m_bkup_top == (UNDO_CAPACITY-1)) ? 0 : (++m_bkup_top));

	m_bkup_array[m_bkup_top].value = value;
	m_bkup_array[m_bkup_top].x = x;
	m_bkup_array[m_bkup_top].y = y;
	m_bkup_array[m_bkup_top].z = z;
	m_bkup_array[m_bkup_top].flag = flag;

	m_can_undo = TRUE;
}

//从地图矩阵读出一个数据
BOOL CHeartEditorDoc::LoadFromBkup(UNDO_ITEM* item)
{
	if (m_bkup_counter > 0){

		*item = m_bkup_array[m_bkup_top];
		m_bkup_top = (m_bkup_top == 0 ? UNDO_CAPACITY - 1 : --m_bkup_top);
		m_bkup_counter--;
		if (m_bkup_counter == 0){
			m_can_undo = FALSE;
		}
		else{
			m_can_undo = TRUE;
		}
		return TRUE;
	}
	else{
		return FALSE;
	}
}

//执行undo操作的函数
void CHeartEditorDoc::Undo(void)
{
	UNDO_ITEM it;
	while (LoadFromBkup(&it)) {

		if (m_nCurImageIndex != it.z) {
			m_nCurImageIndex = it.z;
			UpdateAllViews(NULL);
		}

		m_arOrganMatrix[it.z][it.x][it.y] = it.value;

		//每个操作动作的最后一个flag是1，遇到1说明整个undo操作完成了
		//（写入undo序列时，第一个写入的item的flag是1）
		if (it.flag) { 
			break;
		}
	}
}

BOOL CHeartEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	ZeroMemory (m_arOrganMatrix, sizeof(m_arOrganMatrix));
	return TRUE;
}

void CHeartEditorDoc::SetOrgan(int x, int y, char organ)
{
	if (m_nCurImageIndex < 0) return;

	// 保存旧数据，用于undo操作
	SaveToBkup (m_arOrganMatrix[m_nCurImageIndex][x][y],
				x, y, m_nCurImageIndex, m_operate_flag);

	m_operate_flag = 0;//此标识仅一次性有效

	m_arOrganMatrix[m_nCurImageIndex][x][y] = organ;

	SetModifiedFlag();
	m_nCopyFromPage = -1;//复制之后，不能编辑，任何编辑操作都会使复制失效
}

BYTE CHeartEditorDoc::GetOrgan(int x, int y)
{
	if (m_nCurImageIndex < 0) return 0;
	return m_arOrganMatrix[m_nCurImageIndex][x][y];
}

// CHeartEditorDoc 序列化

void CHeartEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}

#ifdef SHARED_HANDLERS

// 缩略图的支持
void CHeartEditorDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 修改此代码以绘制文档数据
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 搜索处理程序的支持
void CHeartEditorDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 从文档数据设置搜索内容。
	// 内容部分应由“;”分隔

	// 例如:  strSearchContent = _T("point;rectangle;circle;ole object;")；
	SetSearchContent(strSearchContent);
}

void CHeartEditorDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CHeartEditorDoc 诊断

#ifdef _DEBUG
void CHeartEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CHeartEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CHeartEditorDoc 命令


BOOL CHeartEditorDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	CFile file (lpszPathName, CFile::modeReadWrite | CFile::typeBinary);
	UINT nReadSize = file.Read (m_arOrganMatrix, sizeof(m_arOrganMatrix));
	if (nReadSize != sizeof(m_arOrganMatrix))
		return FALSE;

	// 通知 Preview 设置器官选项
	if (theApp.m_pHeartModelPreview) {

		BOOL bChkAtrium = FALSE;
		BOOL bChkConduct = FALSE;
		BOOL bChkAutorhy = FALSE;
		BOOL bChkInsula = FALSE;
		BOOL bChkVessel = FALSE;
		BOOL bChkBlood = FALSE;
		BOOL bChkFat = FALSE;
		BOOL bChkSac = FALSE;
		BOOL bChkTrachea = FALSE;
		BOOL bChkVentri = FALSE;

		int x, y, z;
		for (z = 0; z < MODEL_D; z++) {
			for (y = 0; y < MODEL_H; y++) {
				for (x = 0; x < MODEL_W; x++) {
					switch (GetOrgan(x, y, z))
					{
					case ORGAN_ATRIUM:	bChkAtrium = TRUE;	break;
					case ORGAN_CONDUCT:	bChkConduct = TRUE;	break;
					case ORGAN_AUTORHY:	bChkAutorhy = TRUE;	break;
					case ORGAN_INSULA:	bChkInsula = TRUE;	break;
					case ORGAN_VESSEL:	bChkVessel = TRUE;	break;
					case ORGAN_BLOOD:	bChkBlood = TRUE;	break;
					case ORGAN_FAT:		bChkFat = TRUE;		break;
					case ORGAN_SAC:		bChkSac = TRUE;		break;
					case ORGAN_TRACHEA:	bChkTrachea = TRUE;	break;
					case ORGAN_VENTRI:	bChkVentri = TRUE;	break;
					}
				}
			}
		}

		theApp.m_pHeartModelPreview->UpdateOrganCheck(bChkAtrium,
			bChkConduct,
			bChkAutorhy,
			bChkInsula,
			bChkVessel,
			bChkBlood,
			bChkFat,
			bChkSac,
			bChkTrachea,
			bChkVentri);
	}

	return TRUE;
}


BOOL CHeartEditorDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	CFile file (lpszPathName,
		CFile::modeCreate | CFile::modeReadWrite | CFile::typeBinary);

	if (file.m_hFile != CFile::hFileNull) {
		file.Write (m_arOrganMatrix, sizeof(m_arOrganMatrix));
	}
	else {
		return FALSE;
	}


	m_bFillMode = FALSE;

	m_nCopyFromPage  = -1;

	ResetBkup();

	SetModifiedFlag(0);
	return TRUE;
}


void CHeartEditorDoc::OnExportParaview()
{
	const static TCHAR szTxtFileFilter[] = _T("Paraview Data Files (*.vtk)|*.vtk||");
	CFileDialog dlg(FALSE, _T("vtk"), NULL, OFN_HIDEREADONLY, szTxtFileFilter,
		theApp.GetMainWnd());

	dlg.m_ofn.lpstrTitle = _T("导出当前时序数据");
	if (IDOK != dlg.DoModal()) {
		return;
	}

	BeginWaitCursor();

	CString sFileName = dlg.GetPathName();
	char szFileHead[] = "# vtk DataFile Version 3.0\n"
						"vtk output\n"
						"ASCII\n"
						"DATASET STRUCTURED_POINTS\n"
						"DIMENSIONS 200 242 200\n"
						"SPACING 1 1 1\n"
						"ORIGIN 0 0 0\n"
						"POINT_DATA 9680000\n"
						"SCALARS ImageFile int 1\n"
						"LOOKUP_TABLE default\n";

	//写入文件
	CFile file(sFileName, CFile::modeReadWrite | CFile::modeCreate | CFile::typeBinary);
	file.Write(szFileHead, sizeof(szFileHead)-1);//末尾\0不写


	//写入数据部分
	int code_digit_len = ORGAN_MAX > 10 ? 2 : 1;
	int buffer_len = MODEL_D * MODEL_W * MODEL_H * (code_digit_len + 1);
	char* buffer = new char[buffer_len];
	int i = 0;

	int x, y, z;
	for (y = 0; y < MODEL_H; y++) {
		for (z = MODEL_D-1; z > -1; z--) {
			for (x = 0; x < MODEL_W; x++) {


				char organ = m_arOrganMatrix[z][x][y];
				char code[10];
				sprintf (code, "%d", (int)organ);
				int code_len = strlen (code);
				int t;
				for (t = 0; t < code_len; ) {
					buffer[i ++] = code[t ++];
				}

				buffer[i ++] = ' '; // 添加空格
			}
		}
	}

	file.Write(buffer, i-1);//去掉尾部的空格

	file.Flush();
	file.Close();

	EndWaitCursor();
	theApp.GetMainWnd()->MessageBox(_T("OK，导出成功！"));
}

void CHeartEditorDoc::OnEditCopy()
{
	m_nCopyFromPage = m_nCurImageIndex;
}

void CHeartEditorDoc::OnEditPaste()
{
	if (m_nCopyFromPage > -1) {

		int x, y;
		for (x = 0; x < MODEL_W; x ++) {
			for (y = 0; y < MODEL_H; y ++) {
				
				char organ = m_arOrganMatrix[m_nCopyFromPage][x][y];
				if (organ > ORGAN_ZERO && organ < ORGAN_MAX) {
					m_arOrganMatrix[m_nCurImageIndex][x][y] = organ;
				}
			}
		}

		m_nCopyFromPage = -1;

		UpdateAllViews(NULL);
	}
}

void CHeartEditorDoc::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
	pCmdUI->Enable (m_nCopyFromPage > -1);
}

void CHeartEditorDoc::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
	pCmdUI->Enable (m_can_undo);
}

void CHeartEditorDoc::OnUpdateEditFill(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck (m_bFillMode);
}

void CHeartEditorDoc::OnEditUndo()
{
	this->Undo();
	UpdateAllViews(NULL);
}

void CHeartEditorDoc::OnEditFill()
{
	if (m_bFillMode) {
		m_bFillMode = FALSE;
		SetCursor(m_curArrow);
	}
	else {
		m_bFillMode = TRUE;
		SetCursor(m_curFullFill);
	}
}