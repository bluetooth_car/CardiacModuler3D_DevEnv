#pragma once
#include "afxwin.h"
#include "ColorSelectComboBox.h"


// 视图模式
typedef enum _EnViewpointMode
{
	VP_NORMAL,
	VP_LEFT,
	VP_RIGHT,
	VP_BACK,
	VP_LEFT_FRONT,
	VP_RIGHT_FRONT,
	VP_LEFT_BACK,
	VP_RIGHT_BACK,
} EnViewpointMode;

// CHeartModelPreview 窗体视图

class CHeartModelPreview : public CFormView
{
	DECLARE_DYNCREATE(CHeartModelPreview)

protected:
	CHeartModelPreview();           // 动态创建所使用的受保护的构造函数
	virtual ~CHeartModelPreview();

private:
	void StopDrawThread(void);

public:
	void DrawOragn(CDC* pdc, char organ,
				   int offset_x, int start_y,
				   int x, int y, BOOL half_grid = FALSE);

	void Renew(EnViewpointMode mode = VP_NORMAL);

	void UpdateOrganCheck(BOOL bChkAtrium,
		BOOL bChkConduct,
		BOOL bChkAutorhy,
		BOOL bChkInsula,
		BOOL bChkVessel,
		BOOL bChkBlood,
		BOOL bChkFat,
		BOOL bChkSac,
		BOOL bChkTrachea,
		BOOL bChkVentri);

public:
	EnViewpointMode m_enDrawMode;
	BOOL m_bDrawThreadRun;
	HANDLE m_hDrawThread;

	BOOL m_bChkAtrium;
	BOOL m_bChkConduct;
	BOOL m_bChkAutorhy;
	BOOL m_bChkInsula;
	BOOL m_bChkVessel;
	BOOL m_bChkBlood;
	BOOL m_bChkFat;
	BOOL m_bChkSac;
	BOOL m_bChkTrachea;
	BOOL m_bChkVentri;


public:
	enum { IDD = IDD_MODEL_PREVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CColorSelectComboBox m_cmbOrganType;
	virtual void OnInitialUpdate();
	afx_msg void OnCbnSelchangeCmbCurrentOrgan();
	CButton m_chkAtrium;
	CButton m_chkConduct;
	CButton m_chkAutorhy;
	CButton m_chkInsula;
	CButton m_chkVessel;
	CButton m_chkBlood;
	CButton m_chkFat;
	CButton m_chkSac;
	CButton m_chkTrachea;
	CButton m_chkVentri;
	CStatic m_staPreviewArea;
	afx_msg void OnBnClickedBtnViewpointNormal();
	afx_msg void OnBnClickedBtnViewpointBack();
	afx_msg void OnBnClickedBtnViewpointLeftBack();
	afx_msg void OnBnClickedBtnViewpointLeft();
	afx_msg void OnBnClickedBtnViewpointLeftFront();
	afx_msg void OnBnClickedBtnViewpointRightFront();
	afx_msg void OnBnClickedBtnViewpointRight();
	afx_msg void OnBnClickedBtnViewpointRightBack();
	afx_msg void OnBnClickedBtnStopDraw();
protected:
	afx_msg LRESULT OnDrawFinish(WPARAM wParam, LPARAM lParam);
};


