// macro.h

#ifndef __MACRO_H__
#define __MACOR_H__

// 模型“瓷砖”的宽度和高度
#define ORGAN_TILE_PIXEL_W       (6)
#define ORGAN_TILE_PIXEL_H       (6)

// 模型编辑区，上下左右移动时，“瓷砖”的步进块数
#define EDIT_PAGE_STEP     (20)

// 地图的大小，单位是“瓷砖”，包括编辑区以外看不见的部分
// MAP指模型的一层，注意编辑的视角是从上往下看
#define MODEL_W         (200)//width，相当于人体的左右轴
#define MODEL_H         (200)//height，相当于人体的前后轴
#define MODEL_D         (242)//depth，相当于人体的上下轴
//目前找到的图像有242片从主动脉弓上部开始

#define BKGND_W         (ORGAN_TILE_PIXEL_W * MODEL_W)
#define BKGND_H         (ORGAN_TILE_PIXEL_H * MODEL_H)

// 信息栏的Y位置。信息栏在编辑区下方
#define INFO_BAR_Y         (EDIT_AREA_H + SCROLL_BAR_W + 4)

// 地图与缩略图的比例
#define MAP_TO_THUMBNAIL_SCALE      (5)
#define THUMBNAIL_W                 (MAP_GRID_W * MAP_TO_THUMBNAIL_SCALE)
#define THUMBNAIL_H                 (MAP_GRID_H * MAP_TO_THUMBNAIL_SCALE)

// 视图尺寸
#define VIEW_H                      (800)
#define HEART_LAYTER_VIEW_W         (260)
#define HEART_LAYTER_VIEW_H         (VIEW_H)
#define LAYER_EDIT_VIEW_W           (1160)
#define LAYER_EDIT_VIEW_H           (VIEW_H)
#define HEART_MODEL_VIEW_W          (500)
#define HEART_MODEL_VIEW_H          (VIEW_H)


#endif