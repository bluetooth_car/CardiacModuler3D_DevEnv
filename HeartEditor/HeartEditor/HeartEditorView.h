
// HeartEditorView.h : CHeartEditorView 类的接口
//

#pragma once

class CHeartEditorDoc;
class CHeartEditorView : public CScrollView
{
protected: // 仅从序列化创建
	CHeartEditorView();
	DECLARE_DYNCREATE(CHeartEditorView)
	
// 特性
public:
	CHeartEditorDoc* GetDocument() const;

// 操作
public:
	void SetCurImage(int nImgIndex);
	void SetSelectedOrgan(char organ);

private:
	BOOL run_once;
	CDC  m_dcBkgnd;
	CDC  m_dcSlice;
	CDC  m_dcTemp;

	CPoint m_ptTopLeftTile;

	CString m_sCurImageName;

	BOOL m_bLButtonDown;
	BOOL m_bRButtonDown;

	void PrepareSliceDC(CHeartEditorDoc* pDoc);
	BOOL LoadBkgndImage(CString sImgFilename);

	// 设置一个单元
	void SetOneTile(int x, int y, char organ);

	// 获得一个单元的值
	char GetOrgan(CPoint pt);

	// 填充
	void FullFill(CPoint point, char newOrgan, char oldOrgan);

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~CHeartEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // HeartEditorView.cpp 中的调试版本
inline CHeartEditorDoc* CHeartEditorView::GetDocument() const
   { return reinterpret_cast<CHeartEditorDoc*>(m_pDocument); }
#endif

