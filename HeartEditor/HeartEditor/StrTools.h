// StrTools.h

#ifndef __STRTOOLS_H__
#define __STRTOOLS_H__

#define INVALID_VALUE     (-100)

CString GetFileName(CString& strFullPath);
CString GetFilePath(CString& strFullPath);
CString TrimAll(const CString& rs);
int Split(CStringArray& rAr, LPCTSTR pszSrc, LPCTSTR pszDelimiter);
void Process(CMapStringToString& rDst, CStringArray& rAr);
bool ParseRect(CRect& rRc, CString& rs);
bool IncreaseStringNum(CString& rs);
bool IncreaseStringNumWithout13and4(CString& rs);
bool IsPositiveNum(CString& rs);
CString AtoW(BYTE* asc_str, int len);
int GetAge(CTime& tmBirthday);
CTime StringToTime(CString s);
CString itoNumString2(int n);//将n转化成绘制数字的专用字符串
CString itoNumString3(int n);
CString itoNibpString(int unit, int n);//将n转化成绘制血压的专用字符串
CString itoTempString(int unit, int n);//将n转化成绘制温度的专用字符串
CString itoIconString(int n);//将n转化成绘制图标的专用字符串

class CSmartLock
{
private:
	CCriticalSection& m_cs;
public:
	CSmartLock(CCriticalSection& cs) : m_cs(cs) { m_cs.Lock(); };
	virtual ~CSmartLock() { m_cs.Unlock(); };
};

class CIniInt
{
private:
	CMap<CString, LPCTSTR, int, int> m_mapDict;

public:
	CIniInt(CString sFileContent);
	virtual ~CIniInt();
	int Get(LPCTSTR sKey);
};

class CIniString
{
private:
	CMap<CString, LPCTSTR, CString, LPCTSTR> m_mapDict;

public:
	CIniString(CString sFileContent);
	virtual ~CIniString();
	CString Get(LPCTSTR sKey);
};

#endif //__STRTOOLS_H__