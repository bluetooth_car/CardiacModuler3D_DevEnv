#ifndef __GBLTOOLS_H__
#define __GBLTOOLS_H__


// 组织类型
typedef enum _EnModelOrganCode
{
	ORGAN_ZERO = 0,//0表示空
	ORGAN_ATRIUM,  //心肌（有传导，能发电）
	ORGAN_CONDUCT, //传导组织（只传导，不发电）  
	ORGAN_AUTORHY, //自律组织（自发周期性脉冲）
	ORGAN_INSULA,  //绝缘组织（无传导，例如房室环）
	ORGAN_VESSEL,  //血管
	ORGAN_BLOOD,   //血液
	ORGAN_FAT,     //脂肪
	ORGAN_SAC,     //心包
	ORGAN_TRACHEA, //气管
	ORGAN_VENTRI,  //心室肌（有传导，能发电）
	ORGAN_MAX, //不会出现的一个值
} EnModelOrganCode;

// 10种颜色对应10种组织类型
typedef enum _EnOrganColor
{
	CR_ZERO    = 0x00000000,//ZERO
	CR_ATRIUM  = 0x000000FF,//心房肌
	CR_CONDUCT = 0x0044FF44,//传导组织
	CR_AUTORHY = 0x00FFFF00,//自律组织
	CR_INSULA  = 0x00FFFFFF,//绝缘组织
	CR_VESSEL  = 0x00FF0000,//血管
	CR_BLOOD   = 0x00FF00FF,//血液
	CR_FAT     = 0x0000FFFF,//脂肪
	CR_SAC     = 0x00888888,//心包
	CR_TRACHEA = 0x0022AA00,//气管
	CR_VENTRI  = 0x000000AA,//心室肌
} EnOrganColor;

// 在 3D Preview 窗口，组织侧面的颜色（比正面颜色深）
typedef enum _EnSideColor
{
	SIDE_CR_ZERO    = 0x00000000,//ZERO
	SIDE_CR_ATRIUM  = 0x000000CC,//心房肌
	SIDE_CR_CONDUCT = 0x0022CC22,//传导组织
	SIDE_CR_AUTORHY = 0x00CCCC00,//自律组织
	SIDE_CR_INSULA  = 0x00CCCCCC,//绝缘组织
	SIDE_CR_VESSEL  = 0x00CC0000,//血管
	SIDE_CR_BLOOD   = 0x00CC00CC,//血液
	SIDE_CR_FAT     = 0x0000CCCC,//脂肪
	SIDE_CR_SAC     = 0x00555555,//心包
	SIDE_CR_TRACHEA = 0x00117700,//气管
	SIDE_CR_VENTRI  = 0x00000077,//心室肌
} EnSideColor;


// 获得组织的颜色
COLORREF GetOrganColor (char organ);
COLORREF GetSideColor (char organ);
CBrush& GetOrganBrush (char organ);
CBrush& GetSideBrush (char organ);


#endif 