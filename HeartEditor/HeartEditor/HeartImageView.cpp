// HeartImageView.cpp : 实现文件
//

#include "stdafx.h"
#include "HeartEditor.h"
#include "HeartImageView.h"
#include "HeartEditorView.h"
#include "HeartModelPreview.h"

// CHeartImageView

IMPLEMENT_DYNCREATE(CHeartImageView, CListView)

CHeartImageView::CHeartImageView()
{
	theApp.m_pHeartImageView = this;
	m_lstHeartImage.Create(MODEL_W, MODEL_H, ILC_COLOR32, MODEL_D, 16); // 必不可少  
}

CHeartImageView::~CHeartImageView()
{
}

BEGIN_MESSAGE_MAP(CHeartImageView, CListView)
	ON_WM_DESTROY()
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CHeartImageView::OnNMDblclk)
END_MESSAGE_MAP()


// CHeartImageView 诊断

#ifdef _DEBUG
void CHeartImageView::AssertValid() const
{
	CListView::AssertValid();
}

#ifndef _WIN32_WCE
void CHeartImageView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif
#endif //_DEBUG

BOOL CHeartImageView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style = cs.style | LVS_ICON;

	return CListView::PreCreateWindow(cs);
}
// CHeartImageView 消息处理程序

static BOOL _bOnlyOnce = TRUE;
void CHeartImageView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	if (_bOnlyOnce) {
		_bOnlyOnce = FALSE;

		BeginWaitCursor();

		int i;
		//按顺序读入图片列表
		for (i = 0; i < MODEL_D; i++) 
		{
			CString sImgFilename = theApp.GetImagePathByIndex(i);

			HBITMAP hb = (HBITMAP)LoadImageW(NULL, sImgFilename,
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE);

			CBitmap* pBitmap = new CBitmap;
			pBitmap->Attach(hb);
			m_lstHeartImage.Add(pBitmap, RGB(0,0,0));
			delete pBitmap;
		}

		CListCtrl& list = GetListCtrl();
		list.SetImageList(&m_lstHeartImage, LVSIL_NORMAL);

		LONG lStyle;
		lStyle = GetWindowLong(list.m_hWnd, GWL_STYLE);
		lStyle &= ~LVS_TYPEMASK;
		lStyle |= LVS_SHOWSELALWAYS;
		SetWindowLong(list.m_hWnd, GWL_STYLE, lStyle);

		for (i = 0; i < MODEL_D; i++)
		{
			//LV_ITEM lvitem = {0};
			//lvitem.iImage = i;
			//lvitem.mask = LVIF_IMAGE;

			CString sImgFilename;
			sImgFilename.Format(_T("%03d.bmp"), i+1);
			list.InsertItem(i, sImgFilename , i);
		}

		EndWaitCursor();
	}
}

void CHeartImageView::OnDestroy()
{
	CListView::OnDestroy();

	//清空图片列表
	m_lstHeartImage.DeleteImageList();
}

void CHeartImageView::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    int nItem = pNMItemActivate->iItem;

	CListCtrl& list = GetListCtrl();
    if(nItem >= 0 && nItem < list.GetItemCount())//判断双击位置是否在有数据的列表项上面
    {
		if (theApp.m_pHeartEditorView) {
			theApp.m_pHeartEditorView->SetCurImage(nItem);
		}

		// 不要刷这个图，太慢了
		//if (theApp.m_pHeartModelPreview) {
		//	theApp.m_pHeartModelPreview->Renew();
		//}
    }

    *pResult = 0;
}
