
// HeartEditorDoc.h : CHeartEditorDoc 类的接口
//


#pragma once


#define UNDO_CAPACITY 100000 //设定可以UNDO的格子数

typedef struct _UNDO_ITEM
{
	WORD x;
	WORD y;
	WORD z;
	char value;
	char flag;
} UNDO_ITEM;

class CHeartEditorDoc : public CDocument
{
private:
	BOOL m_can_undo;//是否可以undo
	char m_operate_flag;
	int m_bkup_counter;//定义计数器（undo的次数）
	int m_bkup_top;//定义栈顶指针（undo栈）
	UNDO_ITEM m_bkup_array[UNDO_CAPACITY];//用于存放写入操作的历史数据，undo操作用

	void ResetBkup();//用于清空历史数据，并使m_can_undo指示失效。
	void SaveToBkup(char value, WORD x, WORD y, WORD z, char flag = 0);//向Undo队列写入一个数据
	BOOL LoadFromBkup(UNDO_ITEM* item);//从Undo队列读出一个数据
	void Undo(void);//执行undo操作的函数

public:
	BOOL m_bFillMode;

protected: // 仅从序列化创建
	CHeartEditorDoc();
	DECLARE_DYNCREATE(CHeartEditorDoc)

// 特性
public:
	HCURSOR m_curArrow;
	HCURSOR m_curFullFill;

private:
	int m_nCurImageIndex;
	BYTE m_arOrganMatrix[MODEL_D][MODEL_W][MODEL_H];

	char m_chSelectedOrgan;
	int  m_nCopyFromPage;//当前被复制的页面编号

// 操作
public:
	inline BOOL CanUndo(void) { return m_can_undo; }
	inline void SetOperateFlag(void) { m_operate_flag = 1; }
	inline int GetCurImageIndex(void) {
		return m_nCurImageIndex;
	}
	inline void SetCurImage(int index) {
		m_nCurImageIndex = index;
	}
	inline void SetSelectedOrgan(char organ) {
		m_chSelectedOrgan = organ;
	}
	inline char GetSelectedOrgan(void) {
		return m_chSelectedOrgan;
	}

	void SetOrgan(int x, int y, char organ);
	BYTE GetOrgan(int x, int y);

	inline BYTE GetOrgan(int x, int y, int z) {
		return m_arOrganMatrix[z][x][y];
	}

// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 实现
public:
	virtual ~CHeartEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 用于为搜索处理程序设置搜索内容的 Helper 函数
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	afx_msg void OnExportParaview();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditFill(CCmdUI *pCmdUI);
	afx_msg void OnEditFill();
};
