//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 HeartEditor.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_HeartEditorTYPE             130
#define IDD_MODEL_PREVIEW               311
#define IDC_FULLFILL                    312
#define IDC_CMB_CURRENT_ORGAN           1001
#define IDC_CHK_SHOW_ATRIUM             1002
#define IDC_CHK_SHOW_CONDUCT            1003
#define IDC_CHK_SHOW_AUTORHY            1004
#define IDC_CHK_SHOW_INSULA             1005
#define IDC_CHK_SHOW_VESSEL             1006
#define IDC_CHK_SHOW_BLOOD              1007
#define IDC_CHK_SHOW_FAT                1008
#define IDC_CHK_SHOW_SAC                1009
#define IDC_CHK_SHOW_TRACHEA            1010
#define IDC_CHK_SHOW_VENTRI             1011
#define IDC_STA_PREVIEW                 1012
#define IDC_BTN_VIEWPOINT_BACK          1013
#define IDC_BTN_VIEWPOINT_LEFT_BACK     1014
#define IDC_BTN_VIEWPOINT_LEFT          1015
#define IDC_BTN_VIEWPOINT_LEFT_FRONT    1016
#define IDC_BTN_VIEWPOINT_NORMAL        1017
#define IDC_BTN_VIEWPOINT_RIGHT_FRONT   1018
#define IDC_BTN_VIEWPOINT_RIGHT         1019
#define IDC_BTN_VIEWPOINT_RIGHT_BACK    1020
#define IDC_BTN_STOP_DRAW               1021
#define ID_EXPORT_PARAVIEW              32771
#define ID_EDIT_FILL                    32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        313
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
